**Js-basics**

# Теория

  1. An object's prototype vs. `prototype` property.

  1. Реализованы ли в JavaScript ООП *инкапсуляция*, *наследование* и *полиморфизм*?  Если ДА, то как?

  1. Отличия *прототипной* модели наследования от *классической* (на основе классов)?

  1. Объясните высказвание: ["The prototypal inheritance model itself is, in fact, more powerful than the classic model"](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain).

  1. Есть ли в ES6 классы?

  1. Как соотносятся между собой _функция_ и _конструктор_?

  1. Значения ключевого слова `this` в зависимости от контекста.

# Практические задания

Все задания должны быть покрыты unit-тестами.

**5.01**

Создайте объект, у которого набор _собственных_ свойств совпадает с набором _всех_ его свойств.  Продемонстрируйте на примере метода `toString()`.

**5.02**

Напишите программу, создающую объекты **person** со свойствами `age` и `name`, а также методом `run`, который выводит в консоль строку `'Person <person name> is running ...'`.

Создание объектов реализуйте разными способами, используя:

  - конструктор `Object()`
  - _литеральную (инициирующую) нотацию_ (англ. _literal notation_).
  - фабрика, использующая `Object.create()`
  - пользовательскую функцию-конструктор **Person** и прототипы. `age` и `name` - аргументы конструктора.
  - пользовательский ES6-класс **Person**. `age` и `name` - аргументы конструктора.

**5.03**

Для последнего ~~каждого (кроме первых двух)~~ способа из предыдущего задания реализуйте наследование, **developer** наследует от **person**.  Дополнительное свойство у **developer** - `language`. Дополнительный метод у **developer** - `code()`, который выводит в консоль строку `'Developer <developer name> is coding with <language> ...'`. `age`, `name` и `language` - аргументы конструктора.

~~**5.04**~~

Добавьте в ES6-класс **Person** метод, который по истечении 1 секунды увеличивает значение `age` на 1. Используйте `setTimeout()`, которой в качестве первого аргумента принимает ES5 функцию.

Добавьте в ES6-класс **Developer** метод, который по истечении 1 секунды уменьшает значение `age` на 1. Используйте `setTimeout()`, которой в качестве первого аргумента принимает ES6 arrow функцию.
