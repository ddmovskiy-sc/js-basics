'use strict';

exports.compareProperties = (obj1, obj2) => {
  let result = false;
  if (Object.getOwnPropertyNames(obj1).length !== Object.getOwnPropertyNames(obj2).length) {
    result = false;
  } else {
    for (const key1 in obj1) {
      if (key1 in obj2) {
        result = true;
      } else {
        result = false;
        break;
      }
    }
  }
  return result;
}
