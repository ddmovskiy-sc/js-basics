'use strict';

exports.toArrayLikeObject = (arr) => {
  const arrayLikeObject = {};
  for (const key in arr) {
    if (arr.hasOwnProperty(key)) {
      arrayLikeObject[key] = arr[key];
    }
  }
  arrayLikeObject.length = arr.length;
  return arrayLikeObject;
}
