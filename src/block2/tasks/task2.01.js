'use strict';

exports.showLastNumber = function(number, numberWay) {
  let lastNumber = 0;
  if (numberWay === 1) {
    lastNumber = (number.toString())[(number.toString()).length - 1];
  }
  if (numberWay === 2) {
    lastNumber = (String(number))[String(number).length - 1];
  }
  if (numberWay === 3) {
    if (number >= 10 && (number === Math.trunc(number))) {
      lastNumber = number % 10;
    }
    if (number !== Math.trunc(number)) {
      let floatPart = (number - Math.trunc(number) + 1).toFixed(5);
      while (floatPart !== Math.trunc(floatPart)) {
        floatPart *= 10;
      }
      lastNumber = floatPart % 10;
    }
  }
  return lastNumber;
};
