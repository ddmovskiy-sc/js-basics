'use strict';

exports.castlingProp = (person) => {
  const clone = {};
  if (typeof(person) === "object" && Object.keys(person).length > 0) {
    for (const key in person) {
      if (key !== person[key] && typeof(person[key]) === "string") {
        clone[person[key]] = key;
      } else {
        clone[key] = person[key];
      }
    }
    return clone;
  } else {
    return "Castling didn't happend";
  }
}
