'use strict';

exports.sortObjects = (objectForSort) => {
  const propertyName = objectForSort.sorterFieldName;
  const persons = objectForSort.persons.
    filter(person => person.hasOwnProperty(propertyName)).
    sort((a, b) => {
      if (a[propertyName] < b[propertyName]) {
        return -1;
      }
      if (a[propertyName] > b[propertyName]) {
        return 1;
      }
      if (a[propertyName] === undefined && b[propertyName] !== undefined) {
        return 1;
      }
      if (a[propertyName] !== undefined && b[propertyName] === undefined) {
        return -1;
      } else {
        return 0;
      }
    });
  if (objectForSort.sorterFieldDirection === "DESC") {
    persons.reverse();
  }
  return persons;
};
