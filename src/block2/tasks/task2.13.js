'use strict';

exports.calculateAverageWithArrow = (...numbers) => {
  let averageValue = 0;
  let countArguments = numbers.length;
  for (let i = 0; i < numbers.length; i++) {
    if (typeof(numbers[i]) === "number" && isFinite(numbers[i])) {
      averageValue += numbers[i];
    } else {
      countArguments--;
    }
  }
  return averageValue / countArguments;
}
