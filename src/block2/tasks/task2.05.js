'use strict';

exports.kick = (_ => {
  let goals = 0;
  return () => ++goals;
})();
