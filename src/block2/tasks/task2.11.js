'use strict';

function cloneObject(array) {
  const clone = {};
  for (const key in array) {
    if (typeof(array[key]) === "object" && array[key] !== null) {
      clone[key] = cloneObject(array[key]);
    } else {
      clone[key] = array[key];
    }
  }
  return clone;
}

exports.makeIndArrayLike = (array) => {
  const clone = cloneObject(array);
  clone.length = array.length;
  return clone;
}
