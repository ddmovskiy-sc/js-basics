'use strict';

exports.getKeysObject = (obj) => {
  const arrayKeys = [];
  for (const key in obj) {
    if (typeof(obj[key]) === "function") {
      arrayKeys.push(key);
    }
  }
  return (arrayKeys.length > 0) ? arrayKeys : 0;
}
