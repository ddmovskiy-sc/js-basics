'use strict';

exports.countWeeksInYears = (age, dateNow) => {
  const oldDate = new Date(dateNow.getFullYear() - age, dateNow.getMonth(), dateNow.getDay());
  const countWeeks = (dateNow - oldDate) / (3600 * 1000 * 24 * 7);
  return parseInt(countWeeks, 10);
}
