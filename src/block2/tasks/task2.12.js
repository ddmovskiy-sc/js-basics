'use strict';

exports.calculateAverage = function() {
  let averageValue = 0;
  let countArguments = arguments.length;
  for (let i = 0; i < arguments.length; i++) {
    if (typeof(arguments[i]) === "number" && isFinite(arguments[i])) {
      averageValue += arguments[i];
    } else {
      countArguments--;
    }
  }
  return averageValue / countArguments;
};
