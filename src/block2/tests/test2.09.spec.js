'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.09');

const testObject = {
  lang: "java",
  level: "7",
  age: function() {
    return 18;
  },
  sayHi: function() {
    return "Hello";
  }
};

describe('Task2.09', () => {
  it('The function should return names all methods object', () => {
    assert.sameMembers(impl.getKeysObject(testObject), ["sayHi", "age"]);
    assert.equal(impl.getKeysObject({}), 0);
  });
});
