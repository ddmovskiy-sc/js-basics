'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.14');

describe("Task2.14", () => {
  it("The function should return multiply elements function", () => {
    assert.equal(impl.multiply(3)(4)(5), 60);
  });
});
