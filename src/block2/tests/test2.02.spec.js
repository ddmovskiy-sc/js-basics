'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.02');

describe('Task2.02', () => {
  it('The function should return reverse string', () => {
    assert.equal(impl.replaceLetters("some string"), "gnirts emos");
    assert.notEqual(impl.replaceLetters("hello world"), "world hello");
  });
});
