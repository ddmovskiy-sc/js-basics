'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.13');

describe("Task2.13", () => {
  it("The function should return average value using arrow view function", () => {
    assert.equal(impl.calculateAverageWithArrow(1, 2, 3, 10, -1, "asd", NaN, Infinity, null), 3);
  });
});
