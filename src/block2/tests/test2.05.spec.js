'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.05');

describe('Task2.05', () => {
  it('The function should each time return increment number', () => {
    const count = 7;
    for (let i = 1; i <= count; i++) {
      assert.equal(impl.kick(), i);
    }
  });
});
