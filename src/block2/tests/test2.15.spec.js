'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.15');

const alex = {
  name: "alex",
  age: 30,
  isStudent: undefined
};
const mike = {
  name: "mike",
  age: 20,
  isStudent: true
};
const anna = {
  name: "anna",
  age: undefined,
  isStudent: true
};
const sofia = {
  name: "sofia",
  age: 35,
  isStudent: false
};
const max = {
  name: "max",
  isStudent: false
};
const testObject = {
  persons: [alex, mike, anna, sofia, max],
  sorterFieldName: "age",
  sorterFieldDirection: "ASC"
}

describe("Task2.15", () => {
  it("The function should return sort list objects by sorterFieldName", () => {
    assert.deepEqual(impl.sortObjects(testObject), [mike, alex, sofia, anna]);
    assert.notDeepEqual(impl.sortObjects(testObject), [mike, alex, sofia, anna, max]);
  });
});
