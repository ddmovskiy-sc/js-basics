'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.12');

describe("Task2.12", () => {
  it("The function should return average value", () => {
    assert.equal(impl.calculateAverage(1, 2, 3, 10, -1, "asd", NaN, Infinity, null), 3);
  });
});
