'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.10');

const testArray = [{
  width: "300px"
}, {
  padding: undefined
}, {
  background: "gray"
}];

describe("Task2.10", () => {
  it("The function should return array-like object with change property value", () => {
    const changeProp = function() { testArray[0].width = "555px"};
    assert.changes(changeProp, impl.toArrayLikeObject(testArray)[0], "width");
  });
});
