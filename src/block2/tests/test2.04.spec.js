'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.04');

describe('Task2.04', () => {
  it('The function should calculates amount weeks in age', () => {
    assert.equal(impl.countWeeksInYears(18, new Date(2018, 10, 2)), 938);
  });
});
