'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.01');

describe('Task2.01', () => {
  it('The function should return last number', () => {
    assert.equal(impl.showLastNumber(369, 1), 9);
    assert.equal(impl.showLastNumber(369.657, 2), 7);
    assert.notEqual(impl.showLastNumber(0.93, 3), 5);
    assert.equal(impl.showLastNumber(0, 1), 0);
  });
});
