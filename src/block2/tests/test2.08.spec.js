'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.08');

const person = {
  dima: "dima",
  speciality: "it",
  lang: "lang",
  boolean: true
};
const expectedResult = "Castling didn't happend";

describe('Task2.08', () => {
  it('The function should castling object properties', () => {
    assert.deepEqual(impl.castlingProp(person), { dima: "dima", it: "speciality", boolean: true, lang: "lang" });
    assert.equal(impl.castlingProp("some string"), expectedResult);
    assert.deepEqual(impl.castlingProp({}), expectedResult);
  });
});
