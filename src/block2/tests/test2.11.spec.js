'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.11');

const testArray = [{
  width: "300px"
}, {
  padding: undefined
}, {
  background: "gray"
}];

describe("Task2.11", () => {
  it("The function should return independent array-like object, property value don't change", () => {
    const changeProp = function() {testArray[2].background = "black"};
    assert.doesNotChange(changeProp, impl.makeIndArrayLike(testArray)[2], "background");
  });
});
