'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.07');

const obj1 = {
  level: 5,
  lang: "javascript"
};
const obj2 = {
  lang: "javascript",
  level: 5
};

describe('Task2.07', () => {
  it("The function checks properties of objects for equality", () => {
    assert.isTrue(impl.compareProperties(obj1, obj2), "The properties are equal");
    assert.isFalse(impl.compareProperties({
      name: "dima"
    }, {
      sirname: "dima"
    }), "The properties are not equal");
  });
});
