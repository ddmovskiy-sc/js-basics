'use strict';

const { assert } = require('chai');
const impl = require('../tasks/task2.06');

describe('Task2.06', () => {
  it('The function should return swap numbers', () => {
    assert.equal(impl.replaceNumbers(12, 24), "a:24, b:12");
    assert.notEqual(impl.replaceNumbers(34, 82), "a:34, b:82");
  });
});
