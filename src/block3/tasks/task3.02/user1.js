const exp = require("./banker");

const bank = exp.getBank();
exports.callBanker = () => {
  console.log("User1 start work");
  bank.display();
  console.log("User1 add 10");
  bank.add(10);
  bank.display();
  console.log("User1 reset 0");
  bank.reset();
  bank.display();
  console.log("User1 add 10");
  bank.add(10);
  bank.display();
  console.log("User1 finish work");
};
