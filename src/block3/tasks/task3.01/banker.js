let bank = 0;

exports.display = () => {
  console.log("Bank: " + bank);
};

exports.add = (arg) => {
  bank += arg;
};

exports.reset = () => {
  bank = 0;
};
