exports.getBank = function() {
  let bank = 0;

  return {
    add: function(arg) {
      bank += arg;
    },
    display: function() {
      console.log("Bank: " + bank);
    },
    reset: function() {
      bank = 0;
    },
    balance: function() {
      return bank;
    }
  };
};
