const imp = require("./banker");

const bank = imp.getBank();
exports.callBanker = () => {
  console.log("User2 start work");
  bank.display();
  console.log("User2 add 10");
  bank.add(10);
  bank.display();
  console.log("User2 reset 0");
  bank.reset();
  bank.display();
  console.log("User2 add 10");
  bank.add(10);
  bank.display();
  console.log("User2 finish work");
};
