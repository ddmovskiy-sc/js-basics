const user1 = require("./user1");

user1.callBanker();
if (user1.getCurrentBalance() === 10) {
  const user2 = require("./user2");
  console.log("-----------------");
  user2.callBanker();
}
