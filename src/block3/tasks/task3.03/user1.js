const imp = require("./banker");

const bank = imp.getBank();
exports.callBanker = () => {
  console.log("User1 start work");
  bank.display();
  bank.add(10);
  console.log("User1 add 10");
  bank.display();
  console.log("User1 finish work");
};

exports.getCurrentBalance = () => {
  return bank.balance();
};
