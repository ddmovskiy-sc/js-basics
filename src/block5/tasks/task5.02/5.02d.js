'use strict';

const action = {
  run: () => {
    console.log(`Person ${this.name} is running ...`);
  }
};
function Person(name, age) {
  this.name = name;
  this.age = age;
}
Person.prototype.run = action.run;
const person = new Person("Anna", 21);
module.exports = person;
