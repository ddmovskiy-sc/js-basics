'use strict';

const person = {}; // or new Object()
person.name = "Dima";
person.age = 30;
person.run = () => {
  console.log(`Person ${this.name} is running ...`);
};
module.exports = person;
