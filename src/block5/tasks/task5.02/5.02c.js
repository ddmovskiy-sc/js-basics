'use strict';

function Template() {
  this.name = "default";
  this.age = "default";
}
Template.prototype.run = function() {
  console.log(`Person ${this.name} is running ...`);
};

exports.getPerson = (namePerson) => {
  if (namePerson === "Alex") {
    const person = Object.create(new Template());
    person.name = "Alex";
    person.age = 19;
    return person;
  }
  if (namePerson === "Anna") {
    const person = Object.create(new Template());
    person.name = "Anna";
    person.age = 28;
    return person;
  }
  if (namePerson === "Mike") {
    const person = Object.create(new Template());
    person.name = "Mike";
    person.age = 28;
    return person;
  }
  return null;
}
