"use strict";

class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  run() {
    console.log(`Person ${this.name} is running ...`);
  }
}
module.exports = Person;
