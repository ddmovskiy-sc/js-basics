'use strict';

const person = {
  name: "Alex",
  age: 25,
  run: function() {
    console.log(`Person ${this.name} is running ...`);
  }
};
module.exports = person;
