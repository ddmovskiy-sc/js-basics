'use strict';

const car = Object.create(null);
car.type = "truck";
car.speed = 100;
car.color = "black";

module.exports = car;
