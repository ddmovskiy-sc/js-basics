"use strict";

class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  run() {
    console.log(`Person ${this.name} is running ...`);
  }
}

class Developer extends Person {
  constructor(language, name, age) {
    super(name, age);
    this.language = language;
  }
  code() {
    console.log(`Developer ${name} is coding with ${this.language} ...`);
  }
}
module.exports = Developer;
