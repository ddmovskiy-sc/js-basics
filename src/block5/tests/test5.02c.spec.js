'use strict';

const { assert } = require('chai');
const factory = require('../tasks/task5.02/5.02c');

const person = factory.getPerson("Alex");

console.log(person.name);
console.log(person.age);
person.run();

describe('Task5.02 (object-create)', () => {
  it("The function should check Person properties", () => {
    assert.property(person, 'name');
    assert.property(person, 'age');
    assert.property(person, 'run');
  });
});
