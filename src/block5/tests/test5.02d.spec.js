'use strict';

const { assert } = require('chai');
const person = require('../tasks/task5.02/5.02d');

describe('Task5.02 (function-constructor)', () => {
  it("The function should check Person properties", () => {
    assert.property(person, 'name');
    assert.property(person, 'age');
    assert.property(person, 'run');
  });
});
