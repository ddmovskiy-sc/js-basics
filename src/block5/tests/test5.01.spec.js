'use strict';

const { assert } = require('chai');
const car = require('../tasks/task5.01');

describe('Task5.01', () => {
  it("The function should return false if object has only own properties", () => {
    assert.containsAllKeys(car, ['type', 'speed', 'color']);
    assert.notProperty(car, 'toString');
  });
});
