'use strict';

const { assert } = require('chai');
const Person = require('../tasks/task5.02/5.02e');

const dev = new Person("Alex", "18");

describe('Task5.02 (es6-class)', () => {
  it("The function should check Person properties", () => {
    assert.property(dev, 'name');
    assert.property(dev, 'age');
    assert.property(dev, 'run');
  });
});
