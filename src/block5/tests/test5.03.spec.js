'use strict';

const { assert } = require('chai');
const Developer = require('../tasks/task5.03.js');

const dev = new Developer("java", "Alex", 24);

describe('Task5.03', () => {
  it("The function should check Developer properties", () => {
    assert.property(dev, 'name');
    assert.property(dev, 'age');
    assert.property(dev, 'language');
    assert.property(dev, 'code');
  });
});
