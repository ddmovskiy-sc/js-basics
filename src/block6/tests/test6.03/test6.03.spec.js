'use strict';

const path = require('path');
const files = require('../../task6.03/task6.03.js');
const {
  assert
} = require("chai");

describe('task6.03', () => {
  const expected = 8;
  const dirPath = path.join(__dirname, 'resources/');

  it('Actual and expected values should be equal', async () => {
    const actual = await files.calculateLines(dirPath, 'txt');
    assert.equal(actual, expected);
  });
});
