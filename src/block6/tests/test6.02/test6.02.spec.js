'use strict';

const path = require('path');
const sync = require('../../task6.02/sync.js');
const callback = require('../../task6.02/callback.js');
const promisify = require('../../task6.02/promisify.js');
const asyncAwait = require('../../task6.02/async_await.js');
const {
  expect
} = require("chai");

describe('task6.02', () => {
  const expected = 'Lorem ipsum dolor sit amet...\n';
  const filePath = path.join(__dirname, 'resources', 'text.txt');

  it('Sync. Actual and expected values should be equal', () => {
    expect(sync.readFileSynch(filePath)).to.equal(expected);
  });

  it('Callback. Actual and expected values should be equal', (done) => {
    callback.readFileAsynch(filePath, (error, content) => {
      expect(content).to.equal(expected);
      done();
    });
  });

  it('Promisify. Actual and expected values should be equal', async () => {
    const content = await promisify.readFilePromisify(filePath);
    expect(content).to.equal(expected);
  });

  it('Async/await. Actual and expected values should be equal', async () => {
    const content = await asyncAwait.asyncAwaitRead(filePath);
    expect(content).to.equal(expected);
  });
});
