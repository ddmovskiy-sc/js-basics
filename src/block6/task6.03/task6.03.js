'use strict';

const fs = require("fs");
const util = require('util');
const path = require('path');

const readFile = util.promisify(fs.readFile);
const readDir = util.promisify(fs.readdir);

function getCountLines(content) {
  const arrayFromContent = content.split('\n');
  let countLines = arrayFromContent.length;
  if (arrayFromContent[arrayFromContent.length - 1].length === 0) {
    countLines--;
  }
  return countLines;
}

exports.calculateLines = async (directory, suffix) => {
  let counterLines = 0;
  const searchFiles = [];
  try {
    const nameAllFiles = await readDir(directory);
    for (let i = 0; i < nameAllFiles.length; i++) {
      if ((path.extname(nameAllFiles[i])).substring(1) === suffix) {
        searchFiles.push(`${directory}/${nameAllFiles[i]}`);
      }
    }
    const promises = searchFiles.map(async allPath => {
      const content = await readFile(allPath, 'utf-8');
      const countLinesInContent = getCountLines(content);
      counterLines += countLinesInContent;
      return counterLines;
    });
    await Promise.all(promises);
  } catch (error) {
    console.error(error.message);
    return error;
  }
  return counterLines;
}
