'use strict';

function onRejected(error) {
  console.log(error);
}
const value = Math.random() > 0.5 ? "OK!" : 'Unexpected value!';
Promise.resolve(value).then((value) => console.log(value)).
  catch(onRejected);

const promise = Promise.reject('Error');
promise.then(() => console.log("OK!")).
  catch((error) => console.log(error));
