'use strict';

function onRejected(error) {
  console.log(error.message);
}

new Promise((fulfill, reject) => {
  fulfill('I FIRED');
  reject(new Error('I DID NOT FIRE'));
}).then((value) => console.log(value), onRejected);
