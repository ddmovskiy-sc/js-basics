'use strict';

const PATH_JSON = process.argv[2];

function parsePromised(path) {
  return new Promise((fulfill, reject) => {
    try {
      fulfill(JSON.parse(path));
    } catch (e) {
      reject(e);
    }
  });
}

parsePromised(PATH_JSON).
// eslint-disable-next-line no-undef
  then(() => fulfill).
  catch((e) => console.log(e.message));
