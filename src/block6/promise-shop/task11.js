'use strict';

function all(promise1, promise2) {
  return new Promise((fulfill, reject) => {
    let counter = 0;
    const out = [];

    promise1.then((value) => {
      out[0] = value;
      counter++;
      if (counter >= 2) {
        fulfill(out);
      }
    });

    promise2.then((value) => {
      out[1] = value;
      counter++;
      if (counter >= 2) {
        fulfill(out);
      }
    });
  });
}
// eslint-disable-next-line no-undef
all(getPromise1(), getPromise2()).
  then(console.log);
