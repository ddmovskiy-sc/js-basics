'use strict';

function onReject(error) {
  console.log(error.message);
}

new Promise((fulfill, reject) => {
  setTimeout(() => reject(new Error('REJECTED!')), 300);
}).then(null, onReject);
