'use strict';

new Promise((fulfill, reject) => {
  fulfill('PROMISE VALUE');
}).then((value) => console.log(value));

console.log('MAIN PROGRAM');
