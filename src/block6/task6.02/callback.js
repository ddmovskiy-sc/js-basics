'use strict';

const fs = require('fs');

exports.readFileAsynch = (path, callback) => {
  try {
    return fs.readFile(path, "utf8", callback);
  } catch (error) {
    console.error(error.message);
    return error;
  }
}

