'use strict';

const fs = require('fs');

exports.readFileSynch = (path) => {
  let content;
  try {
    content = fs.readFileSync(path, "utf-8");
  } catch (error) {
    console.error(error.message);
    return error;
  }
  return content;
}
