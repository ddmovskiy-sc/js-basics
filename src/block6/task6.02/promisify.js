'use strict';

const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);
exports.readFilePromisify = async (path) => {
  const content = readFile(path, 'utf8');
  return content.
    then(content => {
      return content;
    }).
    catch(error => {
      console.error(error.message);
      return error;
    });
}
