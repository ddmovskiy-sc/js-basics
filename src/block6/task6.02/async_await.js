'use strict';

const fs = require('fs');
const util = require('util');

exports.asyncAwaitRead = async (path) => {
  let content;
  try {
    const readFile = util.promisify(fs.readFile);
    content = await readFile(path, 'utf8');
  } catch (error) {
    console.error(error.message);
    return error;
  }
  return content;
}

